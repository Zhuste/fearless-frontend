import React, { useEffect, useState } from 'react';

function LocationForm () {
    const [states, setStates] = useState([]);
    const [name, setName] = useState('');
    const [room_count, setRoom] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState(''); //must create a secondary const useState in order to State selected and shown in the hooks

    const nameChange = (event) => {
        const valueName = event.target.value;
        setName(valueName);
    }

    const roomChange = (event) => {
        const valueRoom = event.target.value;
        setRoom(valueRoom);
    }

    const cityChange = (event) => {
        const valueCity = event.target.value;
        setCity(valueCity);
    }

    const stateChange = (event) => {
        const valueState = event.target.value;
        setState(valueState);
    }

    const handleSubmit = async (event) => { //form submittion
        event.preventDefault();
        const data = {};
        data.name = name;
        data.room_count = room_count;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            setName('');
            setRoom('');
            setCity('');
            setState('');
        }
    }
    const fetchData = async () => {
        const newUrl = 'http://localhost:8000/api/states/'; // setting URL
        const urlResponse = await fetch(newUrl);    //calling data with GET from URL
        if (urlResponse.ok) {
            const data = await urlResponse.json();
            setStates(data.states)
            }
        }

    useEffect (() => {
        fetchData();
    }, []);

        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                                                                    {/* <!-- V---' name="same as the id/property in model ' or it will throw error--> */}
                        <input onChange={nameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={roomChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" value={room_count}/>
                        <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={cityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" value={city}/>
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                        <select required onChange={stateChange} id="state" name="state" className="form-select" value={state}>
                            <option value="">Choose a state</option>
                            {states.map(state => {
                                return (
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
}

export default LocationForm;

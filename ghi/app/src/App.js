import logo from './logo.svg';
import './App.css';
import Nav from './Nav.js';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      {/* // makes the container around the table */}
      <div className="container">
        <Routes>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="presentations/new" element={<PresentationForm />} />
          <Route index element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStart] = useState('');
    const [ends, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setPresentations] = useState('');
    const [maxAttendees, setAttendees] = useState('');
    const [location, setLocation] = useState('');

    const nameChange = (event) => {
        const valueName = event.target.value;
        setName(valueName);
    }

    const startChange = (event) => {
        const valueStart = event.target.value;
        setStart(valueStart);
    }

    const endChange = (event) => {
        const valueEnd= event.target.value;
        setEnd(valueEnd);
    }

    const descriptionChange = (event) => {
        const valueDesc = event.target.value;
        setDescription(valueDesc);
    }

    const presentationChange = (event) => {
        const valueDesc = event.target.value;
        setPresentations(valueDesc);
    }

    const attendeesChange = (event) => {
        const valueDesc = event.target.value;
        setAttendees(valueDesc);
    }

    const locationChange = (event) => {
        const valueDesc = event.target.value;
        setLocation(valueDesc);
    }

    const handleSubmit = async (event) => { //form submit
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocations([]);   //set to an empty array because when we try to .map
        }
    }
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';
        const response = await fetch(locationUrl);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect (() => {
        fetchData();
    }, []);

        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={nameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={startChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" value={starts}/>
                        <label htmlFor="starts">Start Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={endChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" value={ends}/>
                        <label htmlFor="ends">End Date</label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea onChange={descriptionChange} placeholder="Type something here..." required type="text" id="description" name="description" className="form-control" rows="8" value={description}></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={presentationChange} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={maxPresentations}/>
                        <label htmlFor="max_presentations">Max Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={attendeesChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={maxAttendees}/>
                        <label htmlFor="max_attendees">Max Attendees</label>
                    </div>
                    <div className="mb-3">
                        <select required onChange={locationChange} id="location" name="location" className="form-select" value={location}>
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.id}>
                                    {location.name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        );
}

export default ConferenceForm;

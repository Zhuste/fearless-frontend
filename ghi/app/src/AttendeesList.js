function AttendeesList(props) {
    return (
        <>
        {/* turns the table into alternating stripes */}
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Conference</th>
                </tr>
            </thead>
            <tbody>
                        {/* vvvv  'attendees 'rfers to index.js line 26 */}
                {props.attendees.map(attendee => {
                    return (
                    <tr key={attendee.href}>
                        <td>{ attendee.name }</td>
                        <td>{ attendee.conference }</td>
                    </tr>
                    )
                    })}
            </tbody>
        </table>
        </>
    );
}

export default AttendeesList;

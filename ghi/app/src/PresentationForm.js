import React, { useEffect, useState } from 'react';

function PresentationForm () {
    const [presenter, setPresenter] = useState('');
    const [presEmail, setPresEmail] = useState('');
    const [compName, setCompName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);

    const presenterChange = (event) => {
        const valuePresenter = event.target.value;
        setPresenter(valuePresenter);
    }

    const emailChange = (event) => {
        const valueEmail = event.target.value;
        setPresEmail(valueEmail);
    }

    const compNameChange = (event) => {
        const valueCompName= event.target.value;
        setCompName(valueCompName);
    }

    const titleChange = (event) => {
        const valueTitle = event.target.value;
        setTitle(valueTitle);
    }

    const synopsisChange = (event) => {
        const valueSynopsis = event.target.value;
        setSynopsis(valueSynopsis);
    }

    const conferenceChange = (event) => {
        const valueConference = event.target.value;
        setConference(valueConference);
    }

    const handleSubmit = async (event) => { //form submit
        event.preventDefault();
        const data = {};
        data.presenter_name = presenter;
        data.presenter_email = presEmail;
        data.company_name = compName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        console.log(data);

        const presentationUrl = `http://localhost:8000${conference}presentations/`; //  to get the string ${conference} to function,
        const fetchConfig = {                                                       //  url needs to be wrapped in ` ` not "" or ''
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation)

         //set to an empty array because when we try to .map
        }
    }
    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }

    useEffect (() => {
        fetchData();
    }, []);

        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input onChange={presenterChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={presenter}/>
                    <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={emailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={presEmail}/>
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={compNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" value={compName}/>
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={titleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={title}/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={synopsisChange} className="form-control" rows="5" id="synopsis" name="synopsis" value={synopsis}></textarea>
                </div>
                <div className="mb-3">
                    <select required onChange={conferenceChange} name="conference" id="conference" className="form-select" value={conference}>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                            return (
                                <option key={conference.href} value={conference.href}>
                                    {conference.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
}

export default PresentationForm;

window.addEventListener("DOMContentLoaded", async () => {
    console.log('monkey');

    const locationUrl = 'http://localhost:8000/api/locations/';
    const response = await fetch(locationUrl);

    if (response.ok) {
        const locationData = await response.json();

        const locationTag = document.getElementById("location");
        const location = locationData.locations //'locations' rfers to 'locations' key in dictionary

        for (let locs of location) {   //setting locs to properties in locations
            const options = document.createElement('option');   //creating option element
            options.value = locs.id;    //setting values
            options.innerHTML = locs.name;
            locationTag.appendChild(options);  // used as this would take in all node objects

        }
    }
    console.log('see');
    const conferenceTag = document.getElementById('create-conference-form');
    console.log('monkey do');
    conferenceTag.addEventListener('submit', async (event) => { //creating eventlistender for when 'submit' is clicked to start process
        event.preventDefault();

        const formData = new FormData(conferenceTag);
        const jsonify = JSON.stringify(Object.fromEntries(formData));   //changing data to json

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: jsonify,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            conferenceTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    })
})


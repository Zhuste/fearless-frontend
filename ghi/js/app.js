

function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-subtitle text-secondary">${location}</p>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-center">
            ${starts} - ${ends}
        </div>
        </div>
    `;
    }

//     const url = 'http://localhost:8000/api/conferences/';
//     const response = await fetch(url);
//     console.log(response);

//     const data = await response.json();
//     console.log(data);

// });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');  //querySelectorAll()  -> selects all classes with the 'col'
    let colIndx = 0;
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error("Network response was not OK")
        } else {
            const data = await response.json();

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts);     // V---"numeric" sets the datetime format to numbers only ie "04" for April etc
                    const starts = start.toLocaleDateString("en-US", {month:"numeric", day:"numeric", year:"numeric"});
                    const end = new Date(details.conference.ends);
                    const ends = end.toLocaleDateString("en-US", {month:"numeric", day:"numeric", year:"numeric"});
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    // const row = document.querySelector('.row');
                    // row.innerHTML += html;
                    const column = columns[colIndx % 3];    //accessing a columns element by its index via the calculation remainder of colIndx / 3
                    column.innerHTML += html;
                    colIndx = (colIndx + 1) % 3;    //updates 'colIndx' value to be remainder of 'colIndx + 1' divided by 3
                                                    //ensures the html content is added to each column in a round-a-robin or wrap?
                    console.log(html);

                    // const detailTag = document.querySelector('.card-text');
                    // detailTag.innerHTML = details.conference.description;

                    // const imageTag = document.querySelector('.card-img-top');
                    // imageTag.src = details.conference.location.picture_url;
                    // console.log(details);
                }
            }
        }
    } catch (e) {
        console.error(e)
        console.log("There has been a problem with your fetch operation;", e);
        const row = document.querySelector(".container");
        row.innerHTML += `
        <div class="alert alert-primary" role="alert">${e}</div>`
    }
});

window.addEventListener('DOMContentLoaded', async () => {
//                          ^---this DOM CAPITALIZED don't forget, trying to camelCase will break code
    console.log('check');
    const newUrl = 'http://localhost:8000/api/states/'; // setting URL
    const urlResponse = await fetch(newUrl);    //calling data with GET from URL
    if (urlResponse.ok) {
        const data = await urlResponse.json();
        console.log('monkey');
        const selectTag = document.getElementById('state'); //creating a TAG element

        for(let state of data.states) { // set and calls data from states response
            const option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }

        console.log('hi');
    }

});

    const formTag = document.getElementById('create-location-form');

    console.log('heyy');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);     // compiles set of key/value pairs and set it to a object
        const json = JSON.stringify(Object.fromEntries(formData));    // retreving data from object

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });
